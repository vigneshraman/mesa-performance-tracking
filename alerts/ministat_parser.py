import logging
import re
import sys
from dataclasses import dataclass
from pathlib import Path
from subprocess import CalledProcessError, run
from tempfile import NamedTemporaryFile
from typing import Optional

from alerts.ministat_results import MinistatMeasurement, MinistatResults
from common.gl_log_section import GitlabSection


def create_temp_file(data, prefix):
    tf = NamedTemporaryFile(prefix=prefix, delete=False)
    results = "\n".join(data)
    Path(tf.name).resolve().write_text(results)

    return tf.name


def find_numbers(input: str, type=float) -> list:
    return [type(n) for n in re.findall(r"-?[\d.]+", input)]


@dataclass
class MinistatParser:
    before: list[str]
    after: list[str]
    confidence_level: float = 80.0
    relative_threshold: float = 1

    def run(self, before=None, after=None):
        before = before or self.before
        after = after or self.after
        before_file = create_temp_file(before, "before")
        after_file = create_temp_file(after, "after")

        stdout = self.run_ministat(before_file, after_file)

        # Remove temporary file, if no exception occurred.
        for f in (before_file, after_file):
            Path(f).unlink()

        return self.parse_ministat_output(stdout)

    def run_ministat(self, before_file, after_file) -> list[str]:
        cmd = [
            "ministat",
            "-c",
            str(self.confidence_level),
            "-A",
            before_file,
            after_file,
        ]
        logging.debug(" ".join(["command:"] + cmd))
        stdout: list[str] = []
        try:
            completed_process = run(cmd, check=True, capture_output=True)
            with GitlabSection("ministat_output", "Ministat output", start_collapsed=True):
                logging.debug(completed_process.stdout)
            stdout = completed_process.stdout.decode().split("\n")
        except CalledProcessError as proc_err:
            logging.error(
                "Ministat command failed with code %s:\n%s",
                proc_err.returncode,
                proc_err.stderr.decode().rstrip("\n"),
            )
            if stdout := proc_err.stdout.decode().rstrip("\n"):
                with GitlabSection(
                    "ministat_output",
                    "Ministat output",
                    start_collapsed=True,
                    log_level="error",
                ):
                    logging.error(stdout)
        return stdout

    def parse_ministat_output(self, stdout: list[str]) -> Optional[MinistatResults]:
        # When there is a statistical change detected by ministat with -A flag, it
        # will output 10 lines.
        if len(stdout) != 10:
            return None

        abs_change, abs_err = find_numbers(stdout[6])
        rel_change, rel_err = find_numbers(stdout[7])

        before_file = MinistatMeasurement(*find_numbers(stdout[3]))
        after_file = MinistatMeasurement(*find_numbers(stdout[4]))

        return MinistatResults(
            abs_change,
            abs_err,
            rel_change,
            rel_err,
            before_file,
            after_file,
        )


if __name__ == "__main__":
    before, after = [p.split(",") for p in sys.argv[1:3]]

    parser = MinistatParser(before, after)
    before_file = create_temp_file(before, "before")
    after_file = create_temp_file(after, "after")
    stdout = parser.run_ministat(before_file, after_file)
    result = parser.parse_ministat_output(stdout)

    print(result)
