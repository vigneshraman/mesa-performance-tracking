adapted_lehr_law = (mean, stddev, log_mean_diff=1.0) => {
    // Using CV's form, see 2.2 from:
    // https://rstudio-pubs-static.s3.amazonaws.com/201750_c17bc51d8553452d997ba4d258b0249f.html#sample-size
    coefficient_of_variation = stddev / mean

    return 16.0 * coefficient_of_variation * coefficient_of_variation / log_mean_diff
}

base = from(bucket: "mesa-perf-v2")
    |> range(start: ${start_time})
    |> filter(fn: (r) => r._field == "frame_time")
    |> group(columns: ["job_name", "trace_name"])
    |> keep(columns: ["job_name", "trace_name", "_value", "_time"])
    // TODO: Remove this filter when piglit measurements is fixed
    |> filter(fn: (r) => r._value > 0)

dev = base
    |> stddev(mode: "population")

avg = base
    |> mean()

sample_sizes = join(tables: {stddev: dev, mean: avg}, on: ["job_name", "trace_name"])
    // Somehow using the difference between natural logarithm of means does not work well
    |> map(fn: (r) => ({r with _value: adapted_lehr_law(mean: r._value_mean, stddev: r._value_stddev)}))
    |> keep(columns: ["job_name", "trace_name", "_value"])
    |> group()

sample_sizes
